#!/usr/bin/env iocsh.bash

require(mrfioc2)
require(cntpstats)
require(ntpshm)
require(essioc)
require(evgevrdata)
require(ptpstats)


epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")
epicsEnvSet("SYS", "YMIR-TS:")
epicsEnvSet("EVR", "EVR-01")
epicsEnvSet("DEV", "Ctrl-$(EVR)")
epicsEnvSet("SYSPV", "$(SYS)$(DEV)")
epicsEnvSet("MRF_HW_DB", "evr-pcie-300dc-univ.db")
epicsEnvSet("PCIID", "5:0.0")


# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# E3 Common databases
#iocshLoad("$(essioc_DIR)/essioc.iocsh")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(mrfioc2_DIR)/evr.iocsh", "EVRDB=$(MRF_HW_DB), P=$(SYSPV), OBJ=$(EVR), PCIID=$(PCIID)")

iocshLoad("$(mrfioc2_DIR)/evrevt.iocsh", "P=$(SYSPV), OBJ=$(EVR)")

# Configure the EVR to write its timestamp into NTP shared memory segment 2
# and load some records to expose the NTP shared memory segment as PVs
time2ntp("$(EVR)", 2)
iocshLoad("$(ntpshm_DIR)/ntpshm.iocsh", "P=$(SYS), R=$(DEV):")

# turn on mrfioc2 time debugging messages
var evrMrmTimeDebug 1

# read data sent from the EVG
iocshLoad("$(evgevrdata_DIR)/evgevrdata.iocsh", "P=$(SYSPV), R=:")


dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":00-", EVR=$(EVR), CODE=21, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":01-", EVR=$(EVR), CODE=22, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")

# Chrony status PVs
iocshLoad("$(cntpstats_DIR)/cntpstats.iocsh","SYS=$(SYS), DEV=$(DEV):")

# PTP status PVs
iocshLoad("$(ptpstats_DIR)/ptpstats.iocsh", "P=$(SYS), R=$(DEV):")


iocInit()

iocshLoad("$(mrfioc2_DIR)/evr.r.iocsh", "P=$(SYSPV), OBJ=$(EVR)")

######### INPUTS #########
# Trig-Ext-Sel changed from "Off" to "Edge", Code-Ext-SP changed from 0 to 10
#dbpf $(SYSPV):UnivIn-0-Lvl-Sel "Active High"
#dbpf $(SYSPV):UnivIn-0-Edge-Sel "Active Rising"
#dbpf $(SYSPV):Out-RB00-Src-SP 61
#dbpf $(SYSPV):UnivIn-0-Trig-Ext-Sel "Edge"
#dbpf $(SYSPV):UnivIn-0-Trig-Back-Sel "Off"
#dbpf $(SYSPV):UnivIn-0-Code-Ext-SP 21
#dbpf $(SYSPV):UnivIn-0-Code-Back-SP 0

#dbpf $(SYSPV):UnivIn-1-Lvl-Sel "Active High"
#dbpf $(SYSPV):UnivIn-1-Edge-Sel "Active Rising"
#dbpf $(SYSPV):Out-RB01-Src-SP 61
#dbpf $(SYSPV):UnivIn-1-Trig-Ext-Sel "Edge"
#dbpf $(SYSPV):UnivIn-1-Trig-Back-Sel "Off"
#dbpf $(SYSPV):UnivIn-1-Code-Ext-SP 22
#dbpf $(SYSPV):UnivIn-1-Code-Back-SP 0

#dbpf $(SYSPV):EvtA-SP.OUT "@OBJ=$(EVR),Code=21" 
#dbpf $(SYSPV):EvtA-SP.VAL 21 
#dbpf $(SYSPV):EvtB-SP.OUT "@OBJ=$(EVR),Code=22" 
#dbpf $(SYSPV):EvtB-SP.VAL 22 
#dbpf $(SYSPV):EvtC-SP.OUT "@OBJ=$(EVR),Code=23" 
#dbpf $(SYSPV):EvtC-SP.VAL 23
#dbpf $(SYSPV):EvtD-SP.OUT "@OBJ=$(EVR),Code=24"
#dbpf $(SYSPV):EvtD-SP.VAL 24
#dbpf $(SYSPV):EvtE-SP.OUT "@OBJ=$(EVR),Code=125"
#dbpf $(SYSPV):EvtE-SP.VAL 125
#dbpf $(SYSPV):EvtF-SP.OUT "@OBJ=$(EVR),Code=14"
#dbpf $(SYSPV):EvtF-SP.VAL 14
#dbpf $(SYSPV):EvtG-SP.OUT "@OBJ=$(EVR),Code=27"
#dbpf $(SYSPV):EvtG-SP.VAL 27
#dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=$(EVR),Code=28"
#dbpf $(SYSPV):EvtH-SP.VAL 28



######### OUTPUTS #########
#Set up delay generator 0 to trigger on event 14
#dbpf $(SYSPV):DlyGen-0-Width-SP 1000 #1ms
#dbpf $(SYSPV):DlyGen-0-Delay-SP 0 #0ms
#dbpf $(SYSPV):DlyGen-0-Evt-Trig0-SP 14

#dbpf $(SYSPV):DlyGen-1-Evt-Trig0-SP 14
#dbpf $(SYSPV):DlyGen-1-Width-SP 2860 #1ms
#dbpf $(SYSPV):DlyGen-1-Delay-SP 0 #0ms

#dbpf $(SYSPV):DlyGen-2-Width-SP 1000 #1ms
#dbpf $(SYSPV):DlyGen-2-Delay-SP 500000 #500ms
#dbpf $(SYSPV):DlyGen-2-Evt-Trig0-SP 125

# 88052496/11073=7952Hz
#dbpf $(SYSPV):PS-0-Div-SP 3144732
#dbpf $(SYSPV):Out-RB02-Src-Pulse-SP "Pulser 1"
#dbpf $(SYSPV):Out-RB03-Src-Pulse-SP "Pulser 2"

######## Sequencer #########
#dbpf $(SYSPV):EndEvtTicks 4

# Load sequencer setup
#dbpf $(SYSPV):SoftSeq-0-Load-Cmd 1

# Enable sequencer
#dbpf $(SYSPV):SoftSeq-0-Enable-Cmd 1

# Select run mode, "Single" needs a new Enable-Cmd every time, "Normal" needs Enable-Cmd once
#dbpf $(SYSPV):SoftSeq-0-RunMode-Sel "Normal"


# Use ticks or microseconds
#dbpf $(SYSPV):SoftSeq-0-TsResolution-Sel "Ticks"

# Select trigger source for soft seq 0, trigger source 0, delay gen 0
#dbpf $(SYSPV):SoftSeq-0-TrigSrc-0-Sel 0

# Commit all the settings for the sequnce
# commit-cmd by evrseq!!! 
#dbpf $(SYSPV):SoftSeq-0-Commit-Cmd "1"

